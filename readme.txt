PSO Toolbox


Purpose
-------

This document introduces the Particle Swarm Optimization (PSO) in Scilab. 

The PSO method, published by Kennedy and Eberhart in 1995, 
is based on a population of points at first stochastically 
deployed on a search field. 
Each member of this particle swarm could be a solution of the optimization 
problem. 
This swarm flies in the search field (of D dimensions) and each member 
of it is attracted by its personal best solution and by the best solution 
of its neighbours. Each particle has a memory storing all data relating to its 
flight (location, speed and its personal best solution). 
It can also inform its neighbours, i.e. communicate its speed and position. 
This ability is known as socialisation. For each iteration, the objective 
function is evaluated for every member of the swarm. 
Then the leader of the whole swarm can be determined: it is the particle 
with the best personal solution. The process leads at the end to the best 
global solution. 

This direct search method does not require any knowledge 
of the objective function derivatives.

The PSO is a meta-heuristic optimization process created by Kennedy and Eberhart in 1995.
Three PSO are implanted in this toolbox : 

 * the "Inertia Weight Model" by Shi & Eberhart in 1998, 
 * the "Radius",
 * the "BSG-Starcraft" by the author.

Sébastien Salmon was a mecatronics research engineer and a PhD. student at the M3M - UTBM. 
He uses the PSO for actuator optimization and inverse parameter identification.
Now, he is creating is own company specialized in optimization.

Please cite the author when using modificated PSO (Radius and/or BSG-Starcraft).
Please contact the author if your are satisfied of this works (or not) and if you find some bugs ;) . 

Dependencies
------------

 * This module depends on the apifun module.

Bibliography
------------

 * Kennedy, J. and Eberhart, R. C. (1995). Particle swarm optimization.  Proc. IEEE Int'l. Conf. on Neural Networks, IV, 1942–1948.  Piscataway, NJ: IEEE Service Center.
 * Shi, Y. and Eberhart, R. C. (1998a). Parameter selection in particle swarm optimization.  In Evolutionary Programming VII: Proc. EP98, New York: Springer-Verlag, pp. 591-600.
 * Shi, Y. and Eberhart, R. C. (1998b).  A modified particle swarm optimizer.  Proceedings of the IEEE International Conference on Evolutionary Computation, 69-73. Piscataway, NJ: IEEE Press.
 * Salmon, S. (May 2012), Caractérisation, identification et optimisation des systèmes mécaniques complexes par mise en œuvre de simulateurs hybrides matériels/logiciels, PhD Thesis, UTBM, Belfort, France.

Authors
-------
 
 * Copyright (C) 2012 - Optimization Command & Control Systems - Sebastien Salmon
 * Copyright (C) 2010 - 2012 - M3M - UTBM - Sebastien Salmon
 * Copyright (C) 2011 - DIGITEO - Michael Baudin

Licence
-------

Released in Creative Commons CC-BY-NC-SA.
http://creativecommons.org/licenses/by-nc-sa/2.0/


