// Copyright (C) 2010 - 2011 - M3M - UTBM - Sebastien Salmon
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CC-BY-NC-SA
// http://creativecommons.org/licenses/by-nc-sa/2.0/

function f=script(x)
    f=sum(x.^2,"c")
endfunction

D = 2;
wmax=0.9; // initial weigth parameter
wmin=0.4; // final weigth parameter
weights=[wmax;wmin];
itmax=100; //Maximum iteration number
c1=2; // knowledge factors for personnal best
c2=2; // knowledge factors for global best
c=[c1;c2];
N=20; // problem dimensions: number of particles
launchp=0.9;
speed_factor=2;
number_raptor=N;
borne_sup=ones(D,1)*100;
borne_inf=-borne_sup;
bounds=[borne_inf,borne_sup];
vitesse_max=0.1*borne_sup;
vitesse_min=0.1*borne_inf;
speed=[vitesse_min,vitesse_max];
radius=1e-6;
n_radius=10;
speedf=2;
nraptor=N;
verbose = 0; // 1 to activate autosave and graphics by default
grand('setsd',0); // the random generator must be initialized
[fopt,xopt,itopt]=PSO_bsg_starcraft_radius(script,bounds,speed,itmax,N,weights,c,..
    launchp,speedf,nraptor,radius,n_radius,verbose);
assert_checkalmostequal ( fopt , 0 , [], 1.e-1 );
assert_checkalmostequal ( xopt , zeros(1,D) , [], 1.e-1 );
//
// Try with verbose
verbose = 1;
lines(0);
grand('setsd',0); 
[fopt,xopt,itopt]=PSO_bsg_starcraft_radius(script,bounds,speed,itmax,N,weights,c,..
    launchp,speedf,nraptor,radius,n_radius,verbose);
assert_checkalmostequal ( fopt , 0 , [], 1.e-1 );
assert_checkalmostequal ( xopt , zeros(1,D) , [], 1.e-1 );
//
// Try with an output function
function stop = myoutputfun ( i , fopt , xopt )
  sx = strcat(string(xopt)," ")
  mprintf("Iter %d, fopt=%e, xopt=[%s]\n",i,fopt,sx)
  stop = %f
  plot(i,fopt,"bo")
endfunction
h = scf();
xtitle("Objective function value vs Iteration","Iteration number","Objective function value");
grand('setsd',0);
[fopt,xopt,itopt]=PSO_bsg_starcraft_radius(script,bounds,speed,itmax,N,weights,c,..
    launchp,speedf,nraptor,radius,n_radius,myoutputfun);
assert_checkalmostequal ( fopt , 0 , [], 1.e-1 );
assert_checkalmostequal ( xopt , zeros(1,D) , [], 1.e-1 );
close(h);
//
// Try with an output function, as a list
function stop = myoutputfun2 ( i , fopt , xopt , h )
  sx = strcat(string(xopt)," ")
  mprintf("Iter %d, fopt=%e, xopt=[%s]\n",i,fopt,sx)
  stop = %f
  plot(xopt(1),xopt(2),"bo")
endfunction
function f=scriptV(x1,x2)
    f=x1.^2 + x2.^2
endfunction

h = scf();
xtitle("Optimum vs Iteration","X1","X2");
x1=linspace(-10,10,100);
x2=x1;
[ XX1 , XX2 ]= meshgrid (x1 , x2 );
Z = scriptV ( XX1 , XX2 );
contour(x1,x2,scriptV,[0.1 1 5 10 50 100]);
grand('setsd',0); 
[fopt,xopt,itopt]=PSO_bsg_starcraft_radius(script,bounds,speed,itmax,N,weights,c,..
    launchp,speedf,nraptor,radius,n_radius,list(myoutputfun2,h));
assert_checkalmostequal ( fopt , 0 , [], 1.e-1 );
assert_checkalmostequal ( xopt , zeros(1,D) , [], 1.e-1 );
close(h);
//
// Try with default options (except itmax, which default is too large for unit tests).
grand('setsd',0);
[fopt,xopt,itopt]=PSO_bsg_starcraft_radius(script,bounds,speed,itmax); 
assert_checkalmostequal ( fopt , 0 , [], 1.e-1 );
assert_checkalmostequal ( xopt , zeros(1,D) , [], 1.e-1 );
//
// Try to set only the verbose option to ON.
grand('setsd',0); 
verbose = 1;
[fopt,xopt,itopt]=PSO_bsg_starcraft_radius(script,bounds,speed,itmax,[],[],[],[],[],[],[],[],verbose);
assert_checkalmostequal ( fopt , 0 , [], 1.e-1 );
assert_checkalmostequal ( xopt , zeros(1,D) , [], 1.e-1 );



