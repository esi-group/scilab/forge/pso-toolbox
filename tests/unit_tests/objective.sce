function f=script(x)
    
    [a,b]=size(x) // should be a Nx1 vector
    
    f_temp=x.^2-a*cos(2*%pi*x) // min value is -N at 0 of N
    f=sum(f_temp)
    
endfunction